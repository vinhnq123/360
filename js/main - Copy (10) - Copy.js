/**
 * Global variables and functions
 */

$(window).load(function () {
   
    $(".main-nav button.navbar-toggle").on('touchstart click', function () {
        $("#bs-example-navbar-collapse-1").css({
            "overflow-y": "auto",
            "max-height": $(window).height() - $(".navbar-header").height()
        });
    });
    
   $("#loading").hide(100);
});
/**
 * Website start here
 */
jQuery(document).ready(function($){

       if ($(window).width() < 992) {
        var nav = document.querySelector("#mainNav");

        new Headroom(nav, {
          tolerance: {
            down : 2,
            up : 5
          },
          offset : 100,
          classes: {
            initial: "slide",
            pinned: "slide--reset",
            unpinned: "slide--up"
          }
        }).init();
       

        
    }
    $('.datetimepicker1').datetimepicker({
        format: 'DD/MM/YYYY'
    });
    if ($(".btn-search").length > 0) {
        $(".btn-search").click(function () {
            if ($(".frm-search").hasClass("active")) {
                $(".frm-search").removeClass("active");
            }
            else {
                $(".frm-search").addClass("active");
                $(".frm-search input").focus();
                return false;
            }
        });
    }
		$('<div id="loading"><img src="images/loading.gif" alt=""></div>').appendTo('body');
	    if ($(window).width() < 992) {
        if ($("#mainNav").length > 0) {
            var nav = document.querySelector("#mainNav");

            new Headroom(nav, {
              tolerance: {
                down : 2,
                up : 5
              },
              offset : 100,
              classes: {
                initial: "slide",
                pinned: "slide--reset",
                unpinned: "slide--up"
              }
            }).init();
        }
        if ($("#mainNavBottom").length > 0) {
        var header = document.querySelector("#mainNavBottom");

			new Headroom(header, {
			  tolerance: {
				down : 2,
				up : 5
			  },
			  offset : 100,
			  classes: {
				initial: "slide",
				pinned: "slide--reset",
				unpinned: "slide--up"
			  }
			}).init();
		}

       
    }
	if($('.bxslider').length > 0){
    	
		$('.bxslider').bxSlider({
			 minSlides: 2,
      maxSlides: 4,
      slideWidth: 234,
      slideMargin: 15
		});
	}
  if($('.bxslider-1').length > 0){
      
    $('.bxslider-1').bxSlider({
      minSlides: 2,
      maxSlides: 5,
      slideWidth: 184,
      slideMargin: 15
    });
  }

  if ($(".scrollTop").length > 0) {
        $(window).scroll(function () {
            var e = $(window).scrollTop();
            if (e > 300) {
                $(".scrollTop").show()
            } else {
                $(".scrollTop").hide()
            }
        });
        $(".scrollTop").click(function () {
            $('body,html').animate({
                scrollTop: 0
            })
        });
    }


});

$(document).click(function () {
   
    $(".frm-search").removeClass("active");
    
});
$('.frm-search').click(function (event) {
    event.stopPropagation();
});