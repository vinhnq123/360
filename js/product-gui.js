$(document).ready(function() {
    $('.fancybox').fancybox();
});

$(document).ready(function() {

    $("#owl-slider-mb").owlCarousel({


        navigation: true,
        pagination: false,
        autoplayHoverPause: true,
        items: 5, //10 items above 1000px browser width
        itemsDesktop: [1170, 5], //5 items between 1000px and 901px
        itemsDesktopSmall: [1062, 4], // betweem 900px and 601px
        itemsTablet: [890, 3], //2 items between 600 and 0
        itemsMobile: [480, 1]

    });
});


//$(document).ready(function () {
//    $("#project-slider").owlCarousel({

//        autoPlay: 3000, //Set AutoPlay to 3 seconds
//        navigation: false,
//        pagination: true,
//        items: 4, //10 items above 1000px browser width
//        itemsDesktop: [1270, 4], //5 items between 1000px and 901px
//        itemsDesktopSmall: [1062, 3], // betweem 900px and 601px
//        itemsTablet: [890, 2], //2 items between 600 and 0
//        itemsMobile: [480, 1]

//    });
//});

$(function() {

    // Slideshow 1
    $(".sliderbanner-pro").responsiveSlides({
        "auto": false,
        maxwidth: 862,
        speed: 800,
        nav: true,
        pause: true,
        prevText: "",
        nextText: ""
    });

});