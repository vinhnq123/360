$(function() {
    // Slideshow 1
    $("#slider").responsiveSlides({
        maxwidth: 1240,
        pause: true,
        speed: 800
    });
});
///News
$(document).ready(function() {
    $("#owl-slider").owlCarousel({
        autoPlay: 3000, //Set AutoPlay to 3 seconds
        navigation: false,
        pagination: true,
        items: 3, //10 items above 1000px browser width
        itemsDesktop: [1270, 3], //5 items between 1000px and 901px
        itemsDesktopSmall: [890, 3], // betweem 900px and 601px
        itemsTablet: [766, 2], //2 items between 600 and 0
        itemsMobile: [480, 1]
    });
});